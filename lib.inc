%define EXIT 60
%define OK_ERROR_CODE 0
%define NULL_TERM 0
%define WRITE 1
%define DESCR_STDOUT 1
%define DIV_NUM 10
%define ZERO_CODE 48
%define NINE_CODE 57
%define MINUS_CODE 45
%define SPACE_CODE 0x20
%define TAB_CODE 0x9
%define ESCAPE_SEQ_CODE 0xA
%define NEXT_SYMB 10
%define FALSE 0
%define TRUE 1
%define READ 0
%define DESC_STDIN 0
%define DESC_STDIN 0
%define ERR_CODE 0 


section .text


; Принимает код возврата и завершает текущий процесс
exit: 
   mov rax, EXIT
   mov rdi, OK_ERROR_CODE
   syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp byte[rdi + rcx], NULL_TERM
        je .ex
        inc rcx
        jmp .loop
    .ex:
        mov rax, rcx
        ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, WRITE
    mov rdi, DESCR_STDOUT
    syscall
    ret



; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rax, WRITE
    mov rdi, DESCR_STDOUT
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ESCAPE_SEQ_CODE
    jmp print_char



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rsp
    mov rax, rdi 
    mov r8, DIV_NUM
    dec rsp
    mov [rsp], byte 0
    
    .loop:
        xor rdx,rdx
        div r8 
        add rdx, ZERO_CODE
        dec rsp
        mov [rsp], dl
        test rax, rax
        jne .loop
    .ex:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsp
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .positive
    neg rdi
    push rdi 
    mov rdi, MINUS_CODE
    call print_char
    pop rdi
    
    .positive:
    jmp print_uint
    


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
     xor rax, rax
     xor rcx, rcx
    .loop:
        mov rdx, [rsi + rcx]
        cmp byte [rdi + rcx], dl
        jne .false
        cmp byte [rdi + rcx], NULL_TERM
        je .true
        inc rcx
        jmp .loop
    .true:
        mov rax, TRUE
        ret
    .false:
        mov rax, FALSE
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push NULL_TERM
    mov rdx, 1 
    mov rax, READ
    mov rsi, rsp 
    mov rdi, DESC_STDIN
    syscall
    pop rax	
    ret 



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r8            
    push r9                  
    push r10                
    mov r8, rdi            
    mov r9, rsi           
    xor r10, r10            
.check_spaces:
    test r10, r10         
    jnz .ex              
.loop:
    cmp r10, r9            
    jae .overflow        
    call read_char        
    cmp al, SPACE_CODE           
    je .check_spaces        
    cmp al, TAB_CODE             
    je .check_spaces        
    cmp al, ESCAPE_SEQ_CODE            
    je .check_spaces        
    test al, al             
    jz .ex                 
    mov [r8+r10], al       
    inc r10                 
    jmp .loop             
.overflow: 
    mov rax, ERR_CODE            
    jmp .return
.ex: 
    mov [r8+r10], byte 0   
    mov rax, r8          
    mov rdx, r10            
.return:    
    pop r10                 
    pop r9                   
    pop r8                 
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax	
    xor rcx, rcx	
    mov r10, NEXT_SYMB		
    .loop:
        xor rdx, rdx		
        mov dl, byte[rdi+rcx]  

        cmp dl, ZERO_CODE
        jl .end			
        cmp dl, NINE_CODE		
        jg .end			
    
        imul rax, r10	
        sub rdx, ZERO_CODE 		
        add rax, rdx  	
          
        inc rcx			
        jmp .loop		
    .end:
        mov rdx, rcx 
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], MINUS_CODE      
    je .minus              
    jmp parse_uint         
    .minus:
        inc rdi                
        call parse_uint         
        test rdx, rdx         
        jz .ex               
        neg rax            
        inc rdx                
    .ex:
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r8, r8      
    push rdi
    push rsi
    push rdx         
    call string_length   
    pop rdx
    pop rsi
    pop rdi
    inc rax                     
    cmp rdx, rax                
    jl .error                   
.loop:
    mov cl, byte[rdi + r8]    
    mov byte[rsi + r8], cl     
    inc r8                      
    cmp r8, rax                
    jl .loop                   
.end:
    dec rax                    
    ret                    
.error: 
    mov rax, ERR_CODE             
    ret

